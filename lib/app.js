import express from 'express';
import bodyParser from 'body-parser';

import {project, run, query, pipeline, plugin, log} from './apps';

const dataDir = process.env.LF_API_DATA_DIR || `${process.cwd()}/data`;

// Instantiate the app
export const router = express.Router({mergeParams: true});

router.use((req, res, next) => {
  if (!req.params.dataDir) req.params.dataDir = dataDir;
  next();
});

// Parse the body as JSON
router.use(bodyParser.json());

router.use('/projects', project);
router.use('/projects/:project/runs', run);
router.use('/projects/:project/logs', log);
router.use('/projects/:project/queries', query);
router.use('/projects/:project/pipelines', pipeline);
router.use('/plugins', plugin);

export default {router};
