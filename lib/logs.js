import {curry, merge, concat, orderBy} from 'lodash/fp';
import {join} from 'path';
import Promise from 'bluebird';

import {init as initDb, list as listDb, read, write, readNode} from './fs-db';
import {show as showRun} from './runs';

export const show = curry((b, p, r, id) =>
  read(join(b, p, 'logs', r), `${id}.json`));

export const init = curry((b, p) => initDb(join(join(b, p, 'logs'))));

export const list = curry((b, p, r) => listDb(join(b, p, 'logs', r)));

export const listAll = curry((b, p) => readNode(join(b, p, 'logs'))
  .then(([, ds]) => Promise.reduce(ds, (memo, c) =>
    // eslint-disable-next-line promise/no-nesting
    listDb(join(b, p, 'logs', c)).then(concat(memo))
  , []))
  .then(orderBy('date', 'desc')));

export const create = curry((b, p, r, id) =>
  initDb(join(b, p, 'logs', r))
  .then(() => showRun(b, p, r))
  .then((run) => write(join(b, p, 'logs', r), `${id}.json`, {
    id,
    project: p,
    date: Date.now(),
    run: r,
    actions: run.actions,
  }))
  .then(() => show(b, p, r, id)));

export const update = curry((b, p, r, id, log) =>
  show(b, p, r, id)
  .then(l => merge(l, log))
  .then(l => write(join(b, p, 'logs', r), `${id}.json`, l)));

export default {
  show,
  init,
  list,
  create,
  listAll,
  update,
};
