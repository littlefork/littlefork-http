import express from 'express';
import {create, list} from '../pipelines';

const router = express.Router({mergeParams: true});

/**
 * @api {get} /projects/:project/pipelines/ projects Request a list of pipelines.
 * @apiName ListPipelines
 * @apiGroup Pipelines
 *
 * @apiSuccess {String[]} projects A list of pipelines.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     ['one', 'two']
 */
router.get('/', (req, res) => {
  const p = req.params.dataDir;
  const pr = req.params.project;

  return list(p, pr).then(qs => res.send(qs));
});

/**
 * @api {post} /projects/:project/pipelines Create a new pipeline.
 * @apiName CreatePipelines
 * @apiGroup Pipelines
 *
 * @apiParam {Object} project A project configuration.
 * @apiParam {String} project.id The unique id of a project.
 *
 * @apiParamExample {json} Request-Example:
 *                  {id: 'unique-id'}
 *
 * @apiSuccess (Success 201) {text} Location URI of created project.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 201 CREATED
 *     Location https://localhost/projects/one
 *     {pipeline Object}
 */
router.post('/', (req, res) => {
  const p = req.params.dataDir;
  const pr = req.params.project;
  const d = req.body;

  return create(p, pr, d.id, d)
    .then((pipeline) => {
      const location = `${req.baseUrl}/${req.app.mountpath}/${pr}/pipelines/${d.id}`;
      res.setHeader('Location', location);
      return res.status(201).send(pipeline);
    });
});

export default router;
