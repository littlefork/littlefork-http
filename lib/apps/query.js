import express from 'express';

import {create, list} from '../queries';

const router = express.Router({mergeParams: true});

/**
 * @api {get} /projects/:project/queries/ projects Request a list of queries.
 * @apiName ListQueries
 * @apiGroup Queries
 *
 * @apiSuccess {String[]} projects A list of queries.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     ['one', 'two']
 */
router.get('/', (req, res) => {
  const p = req.params.dataDir;
  const pr = req.params.project;

  return list(p, pr).then(qs => res.send(qs));
});

/**
 * @api {post} /projects/:project/queries Create a new query.
 * @apiName CreateQueries
 * @apiGroup Queries
 *
 * @apiParam {Object} project A project configuration.
 * @apiParam {String} project.id The unique id of a project.
 *
 * @apiParamExample {json} Request-Example:
 *                  {id: 'unique-id'}
 *
 * @apiSuccess (Success 201) {text} Location URI of created project.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 201 CREATED
 *     Location https://localhost/projects/one
 *     {quwey Object}
 */
router.post('/', (req, res) => {
  const p = req.params.dataDir;
  const pr = req.params.project;
  const d = req.body;

  return create(p, pr, d.id, d)
    .then((query) => {
      const location = `${req.baseUrl}/${req.app.mountpath}/${pr}/queries/${d.id}`;
      res.setHeader('Location', location);
      return res.status(201).send(query);
    });
});

export default router;
