import express from 'express';

import {show, list, listAll} from '../logs';

const router = express.Router({mergeParams: true});

router.get('/', (req, res) => {
  const p = req.params.dataDir;
  const pr = req.params.project;

  return listAll(p, pr).then(rs => res.send(rs));
});

router.get('/:run', (req, res) => {
  const p = req.params.dataDir;
  const pr = req.params.project;
  const r = req.params.run;

  return list(p, pr, r).then(rs => res.send(rs));
});

router.get('/:run/:log', (req, res) => {
  const p = req.params.dataDir;
  const pr = req.params.project;
  const r = req.params.run;
  const l = req.params.log;

  return show(p, pr, r, l).then(rs => res.send(rs));
});

export default router;
