import {map, reduce, merge, concat, flow} from 'lodash/fp';
import express from 'express';
import {utils} from 'littlefork-core';

import {drop} from '../config';

const reduceObj = reduce.convert({cap: false});
const mapObj = map.convert({cap: false});
const {list, load} = utils.plugins;

const router = express.Router({mergeParams: true});

/**
 * @api {get} /plugins Show a list of available plugins.
 * @apiName ListPlugins
 * @apiGroup Plugins
 *
 * @apiSuccess {Object[]} plugins The list of plugins.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [{
 *       name: 'mongodb_store',
 *       config_args: {
 *         'mongodb.uri': {
 *           type: "string",
 *           nargs: 1,
 *           desc: "The MongoDB connection string.",
 *           default: "mongodb://localhost/littlefork"
 *         }
 *       }
 *     }]
 */
router.get('/', (req, res) => {
  const [transformations] = flow([list, load])();
  const data = reduceObj((memo, p, name) =>
    concat(memo, [{
      name,
      description: p.desc,
      config_args: drop(mapObj((k, v) =>
        merge({}, {
          description: k.desc,
          name: v,
          default: k.default,
        }), p.argv)),
      query_arg: p.source ? {description: p.source.desc, name: p.source.name} : null,
    }]), [], transformations);
  res.send(data);
});

export default router;
