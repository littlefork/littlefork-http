import express from 'express';

import {show, list, create, trigger} from '../runs';

const router = express.Router({mergeParams: true});

/**
 * @api {get} /projects/:project/runs Show a list of runs.
 * @apiName ShowRuns
 * @apiGroup Runs
 *
 * @apiParam {String} project The project identifier.

 * @apiSuccess {Object[]} runs A list of all runs.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     [{
 *        a: 23,
 *        b: 42
 *     }]
 */
router.get('/', (req, res) => {
  const p = req.params.dataDir;
  const pr = req.params.project;

  return list(p, pr).then(rs => res.send(rs));
});

/**
 * @api {post} /projects/:project/runs Create a new run.
 * @apiName CreateRun
 * @apiGroup Runs
 *
 * @apiParam {String} project A project id.
 * @apiParam {Object} run A run configuration.
 * @apiParam {String} run.id The unique id of a run.
 * @apiParam {Object} run.config Littlefork configuration.
 * @apiParam {Array} run.queries A list of query objects.
 * @apiParam {Array} run.actions A list of actions used by the UI.
 *
 * @apiParamExample {json} Request-Example:
 *     {
 *       id: 23,
 *       actions: ["queries123", "pipeline234"],
 *       config: {plugins: "twitter_search"},
 *       queries: [{type: "twitter_query", term: "bubu"}]
 *     }
 *
 * @apiSuccess (Success 201) {text} Location URI of the created run.
 * @apiSuccessExample Success-Response:
 *    HTTP/1.1 201 CREATED
 *    Location http://localhost/projects/one/runs/23
 */
router.post('/', (req, res) => {
  const p = req.params.dataDir;
  const pr = req.params.project;
  const d = req.body;

  return create(p, pr, d).then((r) => {
    const location = `${req.baseUrl}/${req.app.mountpath}/${d.id}`;
    res.setHeader('Location', location);
    return res.status(201).send(r);
  });
});

router.get('/:run', (req, res) => {
  const p = req.params.dataDir;
  const pr = req.params.project;
  const r = req.params.run;

  return show(p, pr, r).then(rs => res.send(rs));
});

router.post('/:run', (req, res) => {
  const p = req.params.dataDir;
  const pr = req.params.project;
  const r = req.params.run;

  return trigger(p, pr, r).then((l) => res.status(201).send(l));
});

// router.use('/:run/logs', logRouter);

// GET /logs - List all run instances
// GET /runs/:id/logs - List run instances

export default router;
