import {merge} from 'lodash/fp';
import express from 'express';
import Promise from 'bluebird';

import {create, list, show} from '../projects';
import {list as listRun} from '../runs';

const router = express.Router();

/**
 * @api {get} /projects Request a list of projects.
 * @apiName ListProjects
 * @apiGroup Projects
 *
 * @apiSuccess {String[]} projects A list of projects.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     ['one', 'two']
 */
router.get('/', (req, res) => {
  const p = req.params.dataDir;
  return list(p).then(prs => res.send(prs));
});

/**
 * @api {post} /projects Create a new project.
 * @apiName CreateProjects
 * @apiGroup Projects
 *
 * @apiParam {Object} project A project configuration.
 * @apiParam {String} project.id The unique id of a project.
 *
 * @apiParamExample {json} Request-Example:
 *                  {id: 'unique-id'}
 *
 * @apiSuccess (Success 201) {text} Location URI of created project.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 201 CREATED
 *     Location https://localhost/projects/one
 */
router.post('/', (req, res) => {
  const p = req.params.dataDir;
  const d = req.body;

  return create(p, d.id, d)
    .then((project) => {
      const location = `${req.baseUrl}/${req.app.mountpath}/${d.id}`;
      res.setHeader('Location', location);
      return res.status(201).send(project);
    });
});

/**
 * @api {get} /projects/:project Show the details of a single project.
 * @apiName ShowProjects
 * @apiGroup Projects
 *
 * @apiParam {String} project The project identifier.
 *
 * @apiSuccess {Object} project The details of a project.
 *
 * @apiSuccessExample Success-Response:
 *     HTTP/1.1 200 OK
 *     {
 *        a: 23,
 *        b: 42
 *     }
 */
router.get('/:project', (req, res) => {
  const p = req.params.dataDir;
  const pr = req.params.project;
  return Promise.all([show(p, pr), listRun(p, pr)])
                .spread((ppr, rs) => res.send(merge(ppr, {runs: rs})));
});

export default router;
