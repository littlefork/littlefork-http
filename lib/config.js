import {merge, curry, reject, getOr} from 'lodash/fp';
import dotenv from 'dotenv';

dotenv.config();

// reject plugin configs that the api handles internally
export const drop = reject(a => ['mongodb.uri'].includes(a.name));

export const amend = curry((p, c) => {
  const host = getOr('127.0.0.1', 'LF_MONGO_HOST', process.env);
  const port = getOr(27017, 'LF_MONGO_PORT', process.env);

  return merge(c, {
    mongodb: {
      uri: `mongodb://${host}:${port}/${p}`,
    },
  });
});

export default {
  amend,
  drop,
};
