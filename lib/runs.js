import {curry, merge, startsWith} from 'lodash/fp';
import {join} from 'path';
import {runner} from 'littlefork-core';
import Promise from 'bluebird';
import winston from 'winston';

import {init as initDb, list as listDb, read, write} from './fs-db';
import {create as createLog, update as updateLog} from './logs';

import {amend} from './config';

export const show = curry((b, p, id) =>
  Promise.all([
    read(join(b, p, 'runs', id), 'actions.json'),
    read(join(b, p, 'runs', id), 'config.json'),
    read(join(b, p, 'runs', id), 'queries.json'),
  ]).spread((actions, config, queries) =>
    merge({}, {actions, config, queries})));

export const init = curry((b, p) => initDb(join(b, p, 'runs')));

export const list = curry((b, p) => listDb(join(b, p, 'runs')));

export const create = curry((b, p, {id, actions, config, queries}) =>
  Promise.all([
    write(join(b, p, 'runs', id), 'actions.json', actions),
    write(join(b, p, 'runs', id), 'config.json', config),
    write(join(b, p, 'runs', id), 'queries.json', queries),
  ]).then( // return the saved object
    () => ({id, actions, config, queries})
  ));

export const trigger = curry((b, p, id) =>
  show(b, p, id)
    .then(({config, queries}) => {
      const logOutput = [];
      const run = runner(amend(p, config), queries);

      run.stream.onValue(msg => {
        switch (msg.type) {
          case (startsWith('log_', msg.type) ? msg.type : null):
            winston.info(msg);
            logOutput.push(msg); break;
          default:
            winston.info(msg); break;
        }
      });
      run.stream.onEnd(() => updateLog(b, p, id, run.marker, {stream: logOutput}));
      run.stream.onError(e => { throw e; });

      return Promise.all([createLog(b, p, id, run.marker), run()]);
    }).return({}));

export default {
  show,
  init,
  list,
  create,
  trigger,
};
