import {curry} from 'lodash/fp';
import {join} from 'path';

import {read, write, list as listDb, init as initDb} from './fs-db';

export const init = curry((b, p) => initDb(join(b, p, 'pipelines')));

// String -> String -> Future {}
export const show = curry((b, p, id) => read(join(b, p, 'pipelines'), `${id}.json`));

// String -> Future [String]
export const list = curry((b, p) => listDb(join(b, p, 'pipelines')));

// String -> String -> String -> Json -> Future {}
// Returns created data
export const create = curry((b, pr, id, d) => {
  const t = join(b, pr, 'pipelines');
  return write(t, `${id}.json`, d).then(() => show(b, pr, id));
});

// String -> String -> Future ()
export const update = create;

export default {
  list,
  show,
  create,
  update,
};
