import {curry} from 'lodash/fp';
import {join} from 'path';

import {read, write, list as listDb, init as initDb} from './fs-db';

export const init = curry((b, p) => initDb(join(b, p, 'queries')));

// String -> String -> Future {}
export const show = curry((b, p, id) => read(join(b, p, 'queries'), `${id}.json`));

// String -> Future [String]
export const list = curry((b, p) => listDb(join(b, p, 'queries')));

// String -> String -> String -> Json -> Future {}
// Returns created data
export const create = curry((b, pr, id, d) => {
  const t = join(b, pr, 'queries');
  return write(t, `${id}.json`, d).then(() => show(b, pr, id));
});

// String -> String -> Future ()
export const update = create;

export default {
  list,
  show,
  create,
  update,
};
