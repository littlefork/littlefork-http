import {curry} from 'lodash/fp';
import Promise from 'bluebird';
import {join} from 'path';

import {readNode, read, write} from './fs-db';
import {init as initRuns, list as listRuns} from './runs';
import {init as initLogs} from './logs';
import {init as initP} from './pipelines';
import {init as initQ} from './queries';

// String -> String -> Future {}
export const show = curry((b, p) => read(join(b, p), 'data.json'));

// String -> Future [String]
export const list = b => readNode(b).then(([, ds]) => Promise.map(ds, show(b)));

// String -> String -> Json -> Future Object
// Returns created data
export const create = curry((b, p, d) =>
  write(join(b, p), 'data.json', d)
    .then(() => Promise.all([initRuns(b, p), initLogs(b, p), initP(b, p), initQ(b, p)]))
    .then(() => show(b, p)));

// String -> String -> Future ()
export const update = create;

export const runs = listRuns;

export default {
  list,
  show,
  create,
  update,
  runs,
};
