import {curry, invoke, concat, merge} from 'lodash/fp';
import Promise from 'bluebird';
import fs from 'fs';
import {dirname, join} from 'path';
import mkdirp from 'mkdirp';
import rimraf from 'rimraf';

Promise.promisifyAll(fs);
Promise.promisifyAll(mkdirp);
const rimrafAsync = Promise.promisify(rimraf);

const stat = f => fs.statAsync(f);

const statPredicate = pred => f => stat(f).then(invoke(pred));
const isFile = statPredicate('isFile');
const isDir = statPredicate('isDirectory');

// String -> Future ()
export const createDir = p => mkdirp.mkdirpAsync(p);
// String -> String -> Future ()
export const createFile = curry((f, d) =>
  createDir(dirname(f)).then(() => fs.writeFileAsync(f, d)));

// String -> Future [String]
export const list = p => fs.readdirAsync(p);
// String -> Future [String]
export const listDir = p => list(p).filter(f => isDir(join(p, f)));
// String -> Future [String]
export const listFiles = p => list(p).filter(f => isFile(join(p, f)));

// String -> Future String
export const readFile = f => fs.readFileAsync(f).then(invoke('toString'));
// String -> Future Json
export const readJson = f => readFile(f).then(JSON.parse);
// String -> Json -> Future ()
export const writeJson = curry((f, d) => createFile(f, JSON.stringify(d)));

// String -> Future ()
export const deletePath = rimrafAsync;

// (a -> b) -> String -> Future {}
export const groupBy = curry((fn, p) =>
  list(p)
  .reduce((memo, f) =>
    Promise.resolve(fn(f))
           .then(key => merge(memo, {[key]: concat(memo[key] || [], [f])}))
  , {}));

// String -> Future {dirs: [String], files: [String]}
export const groupByType = p =>
  groupBy(f => isFile(join(p, f)).then(b => (b ? 'files' : 'dirs')), p)
  .then(merge({dirs: [], files: []}));

export default {
  createDir,
  createFile,
  listDir,
  readFile,
  readJson,
  writeJson,
  deletePath,
  groupBy,
  groupByType,
};
