import {curry} from 'lodash/fp';
import Promise from 'bluebird';
import {join} from 'path';
import {createDir, readJson, writeJson, groupByType} from './fs';

/**
   Read a file and return it as parsed JSON.

   `read :: String -> String -> Future Json`

   @param {String} p The path of the node.
   @param {String} f The name of the file.
   @returns {Promise.<Object>} The parsed JSON that was read from the file.
 */
export const read = curry((p, f) => readJson(join(p, f)));

/**
   Initialize a node.

   `init :: String -> Future ()`

   @param {String} p The path of the node.
   @returns {Promise.<Null>} Returns `Unit`, pure side effect.
 */
export const init = createDir;

/**
   Write data into a node. Initialize the node recursively if it doesn't exist.

   `write :: String -> String -> {} -> Future ()`

   @param {String} p The path of the node.
   @param {String} f The name of the file.
   @param {Object} d The data to store.
   @returns {Promise.<Null>} Returns `Unit`, pure side effect.
 */
export const write = curry((p, f, d) =>
  init(p).then(() => writeJson(join(p, f), d)));

/**
   Read a node for all data and sub nodes.

   `readNode :: String -> Future [[Object], [String]]`

   ```
   const [fs, ds] = readNode('data/projects');
   // fs => [{...}, {...}]
   // ds = ['other', 'nodes']
   ```

   @param {String} p The path of the node.
   @returns {Promise.<Array.<Array.<Object>, Array.<String>>>} Returns an array
   tuple with the first element being a list of data units, and the
   second element a list of sub nodes.
 */
export const readNode = p =>
  groupByType(p).then(({files, dirs}) => [files, dirs]);

/**
   List the data in a node.

   `list :: String -> Future [Object]`

   @param {String} p The path of the node.
   @returns {Promise.<Array.<Object>>} A list of data units.
*/
export const list = p => readNode(p).then(([fs]) => Promise.map(fs, read(p)));

export default {
  read,
  write,
  readNode,
  list,
};
