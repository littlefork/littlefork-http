import express from 'express';
import http from 'http';
import winston from 'winston';
import logger from 'express-winston';
import errorHandler from 'errorhandler';
import dotenv from 'dotenv';
import passport from 'passport';
import LocalAPIKeyStrategy from 'passport-localapikey-update';

import {router} from './app';

dotenv.config();

const app = express();
const port = process.env.LF_API_PORT || 8000;

// When true, do a graceful shutdown by refusing new incoming requests.
let gracefullyClosing = false;

passport.use(new LocalAPIKeyStrategy.Strategy(
  (apikey, done) => {
    if (process.env.LF_API_KEY === apikey) {
      return done(null, {});
    }
    return done(null, false);
  }
));

// Configure our node app for all environments
switch (app.get('env')) {
  case 'development':
  case 'staging': {
    app.use(errorHandler());
    break;
  }
  case 'production': {
    app.use(passport.initialize());
    app.use(passport.authenticate('localapikey', { session: false }));
    break;
  }
  default: { break; }
}

// Setup logging.
winston.remove(winston.transports.Console);
winston.add(winston.transports.Console, {timestamp: true, colorize: true});

app.use(logger.logger({
  transports: [new winston.transports.Console({colorize: true})],
  expressFormat: true,
  colorStatus: true,
}));

// Middleware for graceful shutdowns
app.use((req, res, next) => {
  if (!gracefullyClosing) {
    next();
  } else {
    res.setHeader('Connection', 'close');
    res.status(502).send();
  }
});

app.use(logger.errorLogger({
  transports: [new winston.transports.Console({json: true, colorize: true})],
}));

app.use('/', router);

// Instantiate and start the HTTP server.
const httpServer = http.createServer(app).listen(port);

httpServer.on('listening', () =>
  winston.info(`Server started on port ${port}`));

// Gracefully shutdown on SIGTERM
// This might not work very well with websockets.
process.on('SIGTERM', () => {
  gracefullyClosing = true;
  winston.info('Received kill signal (SIGTERM), shutting down gracefully.');

  // waiting for existing connections to close and exit the process
  httpServer.close(() => {
    winston.info('Closed out remaining connections.');
    process.exit();
  });

  setTimeout(() => {
    winston.error('Could not close connections in time, forcefully shutting down.');
    process.exit(1);
  }, 30 * 1000);
});

process.on('uncaughtException', (err) =>
  winston.error('uncaught exception', err, err.stack));
