import {identity, reduce, sortBy, merge, keys, concat, uniq, uniqueId,
        isEqual} from 'lodash/fp';
import {assertForall, array, dict, string} from 'jsverify';
import mock from 'mock-fs';
import {join} from 'path';

import {fsHook, constructFs,
        pathArb, segmentArb,
        assertJsonFile} from './helpers';
import {read, write, readNode, list} from '../lib/fs-db';

const sortAry = sortBy(identity);

describe('fs database', () => {
  it('reads data files', () =>
    assertForall(pathArb, dict(string), (p, d) => {
      mock({[join(p, 'data.json')]: JSON.stringify(d)});
      return read(p, 'data.json').then(isEqual(d)).tap(mock.restore);
    }));

  it('writes data points', () =>
    assertForall(pathArb, dict(string), fsHook((p, d) =>
      write(p, 'data.json', d)
      .then(() => assertJsonFile(join(p, 'data.json'), d)))));

  it('read a node', () =>
    assertForall(array(segmentArb), ps => {
      const [fs, ds] = constructFs(ps);

      mock({xx: merge(fs, ds)});

      return readNode('xx')
        .then(([ffs, dds]) =>
          isEqual(sortAry(ffs), sortAry(keys(fs))) &&
          isEqual(sortAry(dds), sortAry(keys(ds))))
        .tap(mock.restore);
    }));

  it('lists node data', () =>
    assertForall(array(segmentArb), ps => {
      const [fs, ds] = reduce(([xs, ys], f) => {
        const d = {a: uniqueId()};
        return [merge(xs, {[f]: JSON.stringify(d)}), concat(ys, [d])];
      }, [{}, []], sortAry(uniq(ps)));

      mock({xx: fs});

      return list('xx')
        .then(dds => isEqual(sortAry(dds), sortAry(ds)))
        .tap(mock.restore);
    }));
});
