import {flow, map, reduce, drop, first, merge, identity, split, uniq,
        sortBy, groupBy as groupBySync, isEqual, keys} from 'lodash/fp';
import {assertForall, array, dict, json, string} from 'jsverify';
import mock from 'mock-fs';
import {basename} from 'path';

import {fsHook, constructFs,
        segmentArb, pathArb, fileArb,
        assertPath, assertFile, assertJsonFile} from './helpers';
import {createDir, createFile,
        listDir, listFiles,
        readFile, readJson, writeJson,
        groupBy, groupByType, deletePath} from '../lib/fs';

const sortAry = sortBy(identity);

describe('fs layer', () => {
  it('creates nested directories', () =>
    assertForall(pathArb, fsHook(p =>
      createDir(p).then(() => assertPath(p)))));

  it('creates files', () =>
    assertForall(fileArb, string, fsHook((f, d) =>
      createFile(f, d).then(() => assertFile(f, d)))));

  it('lists directories', () =>
    assertForall(array(pathArb), ps => {
      const ds = flow([map(flow([split('/'), drop(1), first])), uniq])(ps);

      mock(reduce((memo, p) => merge(memo, {xx: {[p]: {}}}), {xx: {}}, ds));

      return listDir('xx')
        .then(rs => isEqual(sortAry(rs), sortAry(ds)))
        .tap(mock.restore);
    }));

  it('lists files', () =>
    assertForall(array(segmentArb), ps => {
      const fs = flow(([map(basename), uniq]))(ps);

      mock(reduce((memo, f) => merge(memo, {xx: {[f]: ''}}), {xx: {}}, fs));

      return listFiles('xx')
        .then(rs => isEqual(sortAry(rs), sortAry(fs)))
        .tap(mock.restore);
    }));

  it('reads files', () =>
    assertForall(fileArb, string, (f, d) => {
      mock({[f]: d});

      return readFile(f)
        .then(isEqual(d))
        .tap(mock.restore);
    }));

  it('reads from a JSON file', () =>
    assertForall(fileArb, json, (f, d) => {
      mock({[f]: JSON.stringify(d)});
      return readJson(f).then(isEqual(d)).tap(mock.restore);
    }));

  it('writes to a JSON file', () =>
    assertForall(fileArb, dict(string), fsHook((f, d) =>
      writeJson(f, d).then(() => assertJsonFile(f, d)))));

  it('deletes files', () =>
    assertForall(fileArb, f => {
      mock({[f]: ''});

      return deletePath(f)
        .then(() => !assertPath(f))
        .tap(mock.restore);
    }));

  it('can group by iteratee', () =>
    assertForall(array(segmentArb), ps => {
      const [fs, ds] = constructFs(ps);
      const gs = groupBySync(first, uniq(ps));

      mock({xx: merge(fs, ds)});

      return groupBy(first, 'xx')
        .then(obj => isEqual(sortAry(keys(obj)), sortAry(keys(gs))))
        .tap(mock.restore);
    }));

  it('group by type', () =>
    assertForall(array(segmentArb), ps => {
      const [fs, ds] = constructFs(ps);

      mock({xx: merge(fs, ds)});

      return groupByType('xx')
        .then(({files, dirs}) =>
          isEqual(sortAry(keys(fs)), sortAry(files)) &&
          isEqual(sortAry(keys(ds)), sortAry(dirs)))
        .tap(mock.restore);
    }));
});
